import { Card, Button, Alert } from "react-bootstrap";
import { faKeyboard, faVideo, faCheck, faKiss } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';
import databasedb from "../../../sever/firebase";

const ItemRequest = ({username, email ,room}) => {

    const id_user = email.replaceAll('.', '_').replaceAll('@', '_');

    const checkSize = ref(databasedb, `room/${room}/add/${email}`);
    const deleteAdd = ref(databasedb, `room/${room}/remove/${id_user}/refuse`);
    const dongYDB = ref(databasedb, `room/${room}/add/${email}/addUser`);

    const tuChoi = () => {
        set(deleteAdd, true).then(()=> {
        }).catch(() => {
        });
        set(checkSize, null)
    }

    const dongY = () => {
        set(dongYDB, true).then(()=> {
        }).catch(() => {
        });
    }
    
    return (
        <Card class="shadow p-0.5 mb-1.5 bg-white rounded" style={{ marginTop: '5px', zIndex: '1000' }}>
            <tr>
                <th>
                    <div style={{ paddingLeft: '10px', paddingRight: '10px', textAlign: 'center', color: 'blue', borderRadius: '4px', marginLeft: '10px', marginRight: '10px' }}>
                        {username}
                    </div>
                </th>
                <th>
                    <Button style={{ fontSize: '10px', background: 'none', color: 'red', border: '1px solid red' }} onClick={tuChoi} >Từ chối <FontAwesomeIcon icon={faKiss} style={{ marginLeft: '10px', marginRight: '10px' }} /></Button>

                </th>

                <th>
                    <Button style={{ fontSize: '10px', marginLeft: '10px', background: 'none', color: 'green', border: '1px solid green' }} onClick={dongY} >Đồng ý <FontAwesomeIcon icon={faCheck} style={{ marginLeft: '5px', marginRight: '5px' }}/></Button>
                </th>

            </tr>
        </Card>
    );
}


export default ItemRequest;


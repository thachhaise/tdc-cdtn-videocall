import { faCopy, faShieldAlt, faTimes, faUser, faUserFriends, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../StyleCallPage/Request.css';
import { useHistory, Link } from 'react-router-dom';
import databasedb from "../../../sever/firebase";
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';
import { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import ItemRequest from './ItemRequest';
import { useAuth } from "../../../contexts/AuthContext";

const Request = () => {
    const roomID = window.location.pathname.replaceAll('/meet/', '');
    const { currentUser } = useAuth();
    const id_user = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');
    const db = ref(databasedb, 'users/' + id_user + '/username');
    let username = null;

    onValue(db, (snap) => {
        username = snap.val();
    });

    

    const checkSize = ref(databasedb, `room/${roomID}/add`);
    const [addParticipant, setAddParticipant] = useState([]);
    const checkAdmin = ref(databasedb, `room/${roomID}/admin`);

    useEffect(() => {
        try {
            onValue(checkAdmin, (dataSnap) => {
                const admin = dataSnap.val();
                onValue(checkSize, (snapshot) => {
                    let childData = [];
                    snapshot.forEach((childSnapshot) => {
                        if (childSnapshot.val().addUser == false && admin == currentUser.email) {
                            childData.push(childSnapshot.val());
                        }
                    });
                    setAddParticipant(childData);
                }, {
                    onlyOnce: false
                })
            }, {
                onlyOnce: false
            });
            
        } catch (e) {
        }
    }, []);

    const listAdd = addParticipant.map((number) =>
        <ItemRequest key={number.key} username={number.userName} email={number.email} room={roomID} />
    )

    return (
        <div>
            {listAdd}
        </div>
    )
}

export default Request;
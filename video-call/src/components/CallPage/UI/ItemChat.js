import React from "react";
import Card from "react-bootstrap/Card";
const ItemChat = ({username, message, time}) => {
    return (
    
        <div className="chat-block">
            <div className="sender">
                {username} <small>{time}</small>
            </div>
            <Card class="shadow p-0.5 mb-1.5 bg-white rounded" style={{ height: 'auto',width:'max', paddingLeft: '10px'}}>
            <p className="msg">{message}</p>
            </Card>
        </div>
       
    );
}

export default ItemChat;
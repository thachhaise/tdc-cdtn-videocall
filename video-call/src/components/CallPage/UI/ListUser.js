import React from "react";
import { useEffect, useState, useRef } from "react";
import '../StyleCallPage/ListUser.css';
import { faCommentAlt, faPaperPlane, faTimes, faUserFriends, faListUl } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import ItemUser from "./ItemUser";
import databasedb from "../../../sever/firebase";
import { ref, onValue, set, child } from '@firebase/database';

const ListUser = (props) => {

  let participantKey = Object.keys(props.participants);

  const roomID = window.location.pathname.replaceAll('/meet/', '');
  const adminDB = ref(databasedb, "room/" + roomID + "/admin");
  let admin = "";

  onValue(adminDB, (snap) => {
    admin = snap.val();
  });
  
  const currentUser = props.currentUser
    ? Object.values(props.currentUser)[0]
    : null;
  const participants = participantKey.map((element, index) => {
    const currentParticipant = props.participants[element];
    const isCurrentUser = currentParticipant.currentUser;
    if (isCurrentUser) {
      return null;
    }
    const pc = currentParticipant.peerConnection;
    const remoteStream = new MediaStream();
    let curentIndex = index;
    if (pc) {
      pc.ontrack = (event) => {
        event.streams[0].getTracks().forEach((track) => {
          remoteStream.addTrack(track);
        });
        const videElement = document.getElementById(
          `participantVideo${curentIndex}`
        );
        if (videElement) videElement.srcObject = remoteStream;
      };
    } return (
      <ItemUser
        currentParticipant={currentParticipant}
        currentUse={false}
        showAvatar={currentParticipant}
        admin={admin}
      />
    )
  });


  return (
    <div className="user-container">
      <div className="user-header-tabs">
        <div className="tab">
          <FontAwesomeIcon className="icon" icon={faUserFriends} />
          <p> Danh Sách Thành viên ({participantKey.length})</p>
        </div>
      </div>
      <div className="list-section">
        {participants}
        <ItemUser
          currentParticipant={currentUser}
          currentUse={true}
          showAvatar={currentUser}
          admin={admin}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    participants: state.participants,
    currentUser: state.currentUser,
    stream: state.mainStream,
  };
};

export default connect(mapStateToProps)(ListUser);
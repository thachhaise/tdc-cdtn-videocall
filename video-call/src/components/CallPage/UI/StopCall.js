import { faCopy, faShieldAlt, faTimes, faUser, faUserFriends, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../StyleCallPage/StopCall.scss';
import { useHistory, Link } from 'react-router-dom';
import databasedb from "../../../sever/firebase";
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';
import { useEffect, useState } from "react";
import { useAuth } from "../../../contexts/AuthContext";

const StopCall = (props) => {    
    const roomID = window.location.pathname.replaceAll('/meet/', '');
    const checkSize = ref(databasedb,  `room/${roomID}/status`);
    const history = useHistory();
    let admin = false;
    const [userAdmin, setUserAdmin] = useState(false);
    const { currentUser } = useAuth();

    const stopCall = () => {
        sessionStorage.setItem('reload', 1);
        set(checkSize, false);
    }
    const checkAdmin = ref(databasedb, `room/${roomID}/admin`);

    useEffect(() => {
        onValue(checkAdmin, (snap) => {
            const val = snap.val();
            if (currentUser.email === val) {
                admin = true;
                setUserAdmin(admin);
            }
        });
    }, []);

    const leave = () => {
        if (sessionStorage.reload) {
            sessionStorage.setItem('reload', 1);
        } else {
            sessionStorage.setItem('reload', 1);
        }
        history.replace('/');
    }

    return (
        <div className="stop-block">
            {userAdmin && <div className="add-people-btn" onClick={stopCall}>
                <h3>Kết thúc</h3>
            </div>}
            
            <div className="add-people-btn" onClick={leave}>
                <h3>Rời cuộc họp</h3>
            </div>
        </div>
    )
}

export default StopCall;
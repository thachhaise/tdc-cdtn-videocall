import React from "react";
import ListUser from "./ListUser";
import { Participant } from "./Participant";
import Card from "react-bootstrap/Card";
import { faMicrophoneSlash, faMicrophone, faHandPaper, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import databasedb from "../../../sever/firebase";
import { ref, onValue, set, child } from '@firebase/database';
import { useAuth } from "../../../contexts/AuthContext";

import "../StyleCallPage/ListUser.css";

const ItemUser = (props) => {
    const { currentUser } = useAuth();
    const {
        currentParticipant,
        currentUse,
        showAvatar,
        admin,
    } = props;
    if (!currentParticipant) return <></>;
    const email = currentUser.email;
    let check = false;

    const roomID = window.location.pathname.replaceAll('/meet/', '');
    const db = ref(databasedb, "room/" + roomID + "/add/" + currentParticipant.email);
    const dbStatusRemove = ref(databasedb, "room/" + roomID + "/remove/" + currentParticipant.email + "/remove");

    if (admin === email) {
        check = true;
    }

    const onRemoveUser = () => {
        let r = window.confirm("Xóa thành viên: " + currentParticipant.name);
        if (r == true) {
            set(dbStatusRemove, true);
            set(db, null);
        }
    }

    return (
        <Card class="shadow p-0.5 mb-1.5 bg-white rounded" style={{ height: '3rem', paddingTop: "10px", marginTop: "10px" }}>
            <tr>

                <th >
                    {check &&
                        <FontAwesomeIcon
                            icon={faTimesCircle}
                            title="Muted"
                            onClick={onRemoveUser}
                        />
                    }
                </th>

                <th className="iteamuser-block_show-Avatar">
                    {showAvatar && (
                        <div className="username1s"
                            style={{ background: currentParticipant.avatarColor, borderRadius: "50%" }}
                        >
                            {currentParticipant.name[0]}
                        </div>
                    )}

                </th>
                <th className="iteamuser-block_show-Username">
                    <div className="username2">
                        {currentParticipant.name}
                        {currentUse ? "(You)" : ""}
                    </div>
                </th>

                <th>
                    <div className="item-hand" ></div>
                    {currentParticipant.hand && (
                        <FontAwesomeIcon
                            className="muted2"
                            icon={faHandPaper}
                            title="Hand"
                        />
                    )}
                </th>
                
                <th >
                    {!currentParticipant.audio ? (
                        <FontAwesomeIcon
                            className="muted"
                            icon={faMicrophoneSlash}
                            title="Muted"
                        />
                    ) : (<FontAwesomeIcon
                        className="muted"
                        icon={faMicrophone}
                        title="Muted"
                    />)}
                </th>


            </tr>

        </Card>

    );
}

export default ItemUser;
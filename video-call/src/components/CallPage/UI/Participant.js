import React, { useState, useRef, useEffect } from "react";
import Card from "../../Shared/Card/Card";
import { faHandPaper, faMicrophoneSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../StyleCallPage/Participant.css";
import audios from "../../../ausio";
import firebaseDb from '../../../sever/firebase';
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';

export const Participant = (props) => {
  const audioRef = useRef();
  const roomID = window.location.pathname.replaceAll('/meet/', '');

  const {
    curentIndex,
    currentParticipant,
    hideVideo,
    videoRef,
    showAvatar,
    currentUser,
  } = props;
  if (!currentParticipant) return <></>;

  if (currentParticipant.hand) {
    audioRef.current.currentTime = 0;
    audioRef.current.play();
  }

  return (
    <div className={`participant ${hideVideo ? "hide" : ""}`}>
      <audio src={audios[0].src} ref={audioRef}></audio>
      <Card>
        <video
          ref={videoRef}
          className="video"
          id={`participantVideo${curentIndex}`}
          autoPlay
          playsInline
        ></video>
        {!currentParticipant.audio && (
          <FontAwesomeIcon
            className="muted"
            icon={faMicrophoneSlash}
            title="Muted"
          />
        )}
        {showAvatar && (
          <div
            style={{ background: currentParticipant.avatarColor }}
            className="avatar"
          >
            {currentParticipant.name[0]}
          </div>
        )}
        <div className="name">
          {currentParticipant.name}
          {currentUser ? "(You)" : ""}
        </div>

        {currentParticipant.hand && (
          <FontAwesomeIcon
            className="muted1"
            icon={faHandPaper}
            title="Hand"
          />
        )}
      </Card>
    </div>
  );
};

import React, { useEffect, useState, useRef } from 'react'
import '../StyleCallPage/record.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  faDownload
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Pill from "./Pill";
import useScreenRecorder from "use-screen-recorder";
import {
  useReactMediaRecorder,
  ReactMediaRecorder
} from "react-media-recorder";
import { saveAs } from 'file-saver'


const Record = ({ startRecording, mediaBlobUrl, resetRecording, status, stopRecording, mode, setMode, downloadVideo, clearBlobUrl, resumeRecording, pauseRecording }, { props }) => {


  const videoRef = useRef();
  return (
    <div className="record">
      <div className="row">
        <div className="row_record">
          <div className="col-md-3">

            <button className="btn btn-success mode-record">
              {mode}
            </button>
          </div>
          <div className="col-md-9">

            <div class="btn-group">
              <button className="btn btn-info" type="button" onClick={() => setMode("audio")}>
                audio
              </button>
              <button className="btn btn-info" type="button" onClick={() => setMode("video")}>
                video
              </button>
              <button className="btn btn-info" type="button" onClick={() => setMode("screen")}>
                screen
              </button>
            </div>
            {/* {status} */}
            <div className="buttons">
              {(status === "idle" || status === "error") && (
                <button className="btn btn-danger" onClick={startRecording}>Start recording</button>
              )}
               {(status === "recording" || status === "error") && (
                <button className="btn-recordps btn-danger" onClick={pauseRecording}>pauseRecording </button>
              )}
               {(status === "paused") && (
                <button className="btn btn-danger" onClick={resumeRecording}>resumeRecording</button>
              )}
              {(status === "recording" || status === "stopping") && (
                <button  className="btn-recordps btn-danger"  onClick={stopRecording}>Stop recording</button>
              )}
              
              {status === "stopped"  && (
                <button className="btn btn-danger"
                  onClick={ clearBlobUrl}
                >
                  Reset recording
                </button>
              )}
            </div>
          </div>
        </div>

        <Pill
          style={{ flexGrow: 1 }}
          title="Link Xem Trước"
          value={mediaBlobUrl || "Waiting..."}
        />
        <button onClick={downloadVideo}> <FontAwesomeIcon className="icon red" icon={faDownload} /></button>

      </div>


    </div>


  )

}
export default Record;
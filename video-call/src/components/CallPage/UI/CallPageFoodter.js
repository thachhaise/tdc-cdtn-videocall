import {
    faAngleUp, faDesktop, faMicrophone,
    faVideo,
    faVideoSlash,
    faMicrophoneSlash,
    faPhone, faRecordVinyl, faStop, faSms, faList, faHandHolding, faHandHoldingUsd, faHandLizard, faHandRock, faHandPointUp, faHandScissors, faHandPointDown, faHandPaper, faHandshakeSlash, faHandPeace, faHandSparkles
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import '../StyleCallPage/CallPageFoodter.scss';
import React, { useEffect, useState,useRef   } from "react";
import databasedb from "../../../sever/firebase";
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';
import { icon } from "@fortawesome/fontawesome-svg-core";
import ChatIcon from '@mui/icons-material/Chat';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import Badge from '@mui/material/Badge';

const CallPageFoodter = (props) => {
    const [currentTime, setCurrentTime] = useState(0);
    let roomID = window.location.pathname.replaceAll('/meet/', '');
    let sl =props.participantKey.length;
    const [streamState, setStreamState] = useState({
        mic: true,
        video: false,
        screen: false,
        hand: false,
    });


    const micClick = () => {
        setStreamState((currentState) => {
            return {
                ...currentState,
                mic: !currentState.mic,
            };
        });
    };

    const recordclick = () => {
        if (props.record) {  props.setrecord(false); }
        else { props.setrecord(true); }
    };
    
    const onScreenClick = () => {
        props.onScreenClick(setScreenState);
    };

    const setScreenState = (isEnabled) => {
        setStreamState((currentState) => {
            return {
                ...currentState,
                screen: isEnabled,
            };
        });
    };

    const onVideoClick = () => {
        setStreamState((currentState) => {
            return {
                ...currentState,
                video: !currentState.video,
            };
        });
    };

    const onHandClick = () => {
        setStreamState((currentState) => {
            return {
                ...currentState,
                hand: !currentState.hand,
            };
        });
    };

    const onStop = () => {
        if (props.stopCall) {  props.setStopCall(false); }
        else { props.setStopCall(true); }
    }


    useEffect(() => {
        props.onMicClick(streamState.mic);
    }, [streamState.mic]);
    useEffect(() => {
        props.onVideoClick(streamState.video);
    }, [streamState.video]);

    useEffect(() => {
        props.onHandClick(streamState.hand);
    }, [streamState.hand]);

     const setMessengerClick = () => {
        if (props.messenger) {
            props.setMessenger(false);
        } else {
            props.setdsuser(false);
            props.setMessenger(true);
        }
    }
    const setListUserClick = () => {
        if (props.dsuser) {
          
            props.setdsuser(false);
        } else {
            props.setMessenger(false);
            props.setdsuser(true);
        }
    }

    return (
        <div className="foodter-item">
            <div className="center-item">
                <div className={"icon-block"}
                    data-tip={streamState.mic ? "Mute Audio" : "Unmute Audio"}
                    onClick={micClick}>
                    <FontAwesomeIcon className="icon" icon={!streamState.mic ? faMicrophoneSlash : faMicrophone}
                        title="Mute" />
                </div>
                <div className={"icon-block"}
                    data-tip={streamState.video ? "Hide Video" : "Show Video"}
                    onClick={onVideoClick}>
                    <FontAwesomeIcon className="icon" icon={!streamState.video ? faVideoSlash : faVideo} />
                </div>

                <div className="icon-block" data-tip={streamState.video ? "Hide Video" : "Show Video"}
                    onClick={onScreenClick}
                >

                    <FontAwesomeIcon className="icon" icon={faDesktop} />
                </div>
                <div className="icon-block" onClick={onStop}>
                    <FontAwesomeIcon className="icon red" icon={faPhone} />
                </div>

                <div className="icon-block" onClick={recordclick}>
                    <FontAwesomeIcon className="icon red" icon={faRecordVinyl} />
                </div>
                {/* <div className="icon-block" onClick={onHandClick}>
                data-tip={streamState.mic ? "Mute Audio" : "Unmute Audio"}
                    <FontAwesomeIcon className="icon" icon={faHandPaper} />
                </div> */}

                <div className={!streamState.hand ? "icon-block " : "icon-block bac"}

                    data-tip={streamState.hand ? true : false}
                    onClick={onHandClick}>
                    <FontAwesomeIcon className={!streamState.hand ? "icon" : "icon blue"} icon={faHandPaper}
                        title="Mute" />
                </div>
                <div className="icon-block"
                    onClick={setMessengerClick}>
                   <Badge badgeContent={0} color="primary">
                        <ChatIcon color="action" />
                    </Badge>
                </div>
                <div className="icon-block"
                    onClick={setListUserClick}>
                    <Badge badgeContent={sl} color="success" >
                     <FormatListBulletedIcon color="action" />
                    </Badge>
                </div>
            </div>

            <div className="right-item">

            </div>
        </div>
    )
}

export default CallPageFoodter;
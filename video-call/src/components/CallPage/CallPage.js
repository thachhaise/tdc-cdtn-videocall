import React, { useRef, useEffect, useState } from "react";
import './StyleCallPage/CallPage.scss';
import CallPageFoodter from './UI/CallPageFoodter';
import firebaseDb from '../../sever/firebase';
import { useHistory, Link } from 'react-router-dom';

import Participants from "./UI/Participants";
import { connect } from "react-redux";
import { setMainStream, updateUser } from "../../store/actioncreator";
import Messenger from "./UI/Messenger";
import Record from "./UI/Record";
import useScreenRecorder from "use-screen-recorder";
import { ReactMediaRecorder, useReactMediaRecorder } from "react-media-recorder";
import { saveAs } from 'file-saver';
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';
import ListUser from "./UI/ListUser";
import StopCall from "./UI/StopCall";
import Request from "./UI/Request";
import { useAuth } from "../../contexts/AuthContext";

const CallPage = (props) => {
    const roomID = window.location.pathname.replaceAll('/meet/', '');
    const checkSize = ref(firebaseDb, "room/" + `${roomID}/status`);
    const history = useHistory()
    const [mode, setMode] = useState("audio");

    const {
        status,
        error,
        startRecording,
        pauseRecording,
        resumeRecording,
        isMuted,
        muteAudio,
        unMuteAudio,
        stopRecording,
        mediaBlobUrl,
        clearBlobUrl,
        previewStream
    } = useReactMediaRecorder({
        ...(mode === "audio" && { audio: true }),
        ...(mode === "video" && { video: true }),
        ...(mode === "screen" && { screen: true }),
        askPermissionOnMount: true,
        onStop: (blobUrl, blob) => console.info(blobUrl, blob)
    });;
    const downloadVideo = () => {
        if (mediaBlobUrl) {
            saveAs(mediaBlobUrl, `Video-${Date.now()}.webm`)
        }
    }

    const participantRef = useRef(props.participants);

    const currentUser = props.currentUser
        ? Object.values(props.currentUser)[0]
        : null;

    const onMicClick = (micEnabled) => {
        if (props.stream) {
            props.stream.getAudioTracks()[0].enabled = micEnabled;
            props.updateUser({ audio: micEnabled });
        }
    };
    const onVideoClick = (videoEnabled) => {
        if (props.stream) {
            props.stream.getVideoTracks()[0].enabled = videoEnabled;
            props.updateUser({ video: videoEnabled });
        }
    };

    const onHandClick = (handEnabled) => {
        if (props.stream) {
            props.updateUser({ hand: handEnabled });
        }
    };

    useEffect(() => {
        participantRef.current = props.participants;
    }, [props.participants]);

    for (let key in participantRef.current) {
        const sender = participantRef.current[key];
    }

    const updateStream = (stream) => {
        for (let key in participantRef.current) {
            const sender = participantRef.current[key];
            if (sender.currentUser) continue;
            const peerConnection = sender.peerConnection
                .getSenders()
                .find((s) => (s.track ? s.track.kind === "video" : false));
            peerConnection.replaceTrack(stream.getVideoTracks()[0]);
        }
        props.setMainStream(stream);
    };

    const onScreenShareEnd = async () => {
        const localStream = await navigator.mediaDevices.getUserMedia({
            audio: true,
            video: true,
        });

        localStream.getVideoTracks()[0].enabled = Object.values(
            props.currentUser
        )[0].video;

        updateStream(localStream);

        props.updateUser({ screen: false });
    };

    const onScreenClick = async () => {
        let mediaStream;
        if (navigator.getDisplayMedia) {
            mediaStream = await navigator.getDisplayMedia({ video: true });
        } else if (navigator.mediaDevices.getDisplayMedia) {
            mediaStream = await navigator.mediaDevices.getDisplayMedia({
                video: true,
            });
        } else {
            mediaStream = await navigator.mediaDevices.getUserMedia({
                video: { mediaSource: "screen" },
            });
        }

        mediaStream.getVideoTracks()[0].onended = onScreenShareEnd;

        updateStream(mediaStream);

        props.updateUser({ screen: true });
    };


    let irecord = false
    const [record, setrecord] = useState(false);
    useEffect(() => {
        if (irecord) {
            setrecord(true);
        }
    }, []);

    let isStop = false
    const [stopCall, setStopCall] = useState(false);
    useEffect(() => {
        if (isStop) {
            setStopCall(true);
        }
    }, []);

    useEffect(() => {
        if (error) console.error(error);
    }, [error]);

    const iuser = false;
    const [dsuser, setdsuser] = useState(false);
    useEffect(() => {
        if (iuser) {
            setdsuser(true);
        }
    }, []);
    const [messenger, setMessenger] = useState(false);
    const sms = false;
    useEffect(() => {
        if (sms) {
            setdsuser(true);
        }
    }, []);

    // const { currentUser } = useAuth();
    // const id_user = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');

    const db = ref(firebaseDb, "room/" + roomID);
    const db1 = ref(firebaseDb, "room/" + roomID + "/participants");
    // const removeUser = ref(firebaseDb, "room/" + roomID + "/remove/" + id_user);
    let participantKey = Object.keys(props.participants);

    useEffect(() => {
        try {
            onValue(db1, (snap) => {
                const dataMen = snap.val();
                if (snap.val() != null) {
                    onValue(checkSize, (snapshot) => {
                        if (typeof (Storage) !== 'undefined') {
                            if (snapshot.val() == false || snapshot.val() == null) {
                                if (sessionStorage.reload) {
                                    sessionStorage.setItem('reload', 1);
                                } else {
                                    sessionStorage.setItem('reload', 1);
                                }
                                history.replace('/');
                                set(db1, null);
                            }
                        } else {
                            history.replace('/');
                            set(db, null);
                        }
                    }, {
                        onlyOnce: false
                    })
                }
            }, {
                onlyOnce: false
            })

            onValue(db1, (snapshot) => {
                snapshot.forEach((childSnapshot) => {
                    const childKey = childSnapshot.key;
                    onValue(child(db1, childKey + "/userName"), (snap) => {
                        if (!snap.val()) {
                            set(child(db1, childKey), null);
                        }
                    });
                });
            }, {
                onlyOnce: false
            })

        } catch (e) {

        }

    }, []);

    if (props.isLoading) {
        return (
          <div className="callpage-container">
            <div className="bg">
              <div class="loader-container">
                <div class="loader"></div>
              </div>
            </div>
          </div>
        )
      }

    return (
        <div className="callpage-container">
            <div className="main-screen">
                <Participants />
            </div>

            {stopCall && <StopCall />}

            {/* <CallPageHeader /> */}
            <CallPageFoodter
                onScreenClick={onScreenClick}
                onMicClick={onMicClick}
                onVideoClick={onVideoClick}
                setMessenger={setMessenger}
                messenger={messenger}
                setrecord={setrecord}
                stopCall={stopCall}
                setStopCall={setStopCall}
                setmode={setMode}
                record={record}
                setdsuser={setdsuser}
                onHandClick={onHandClick}
                dsuser={dsuser}
                participantKey ={participantKey} />

            {dsuser && <ListUser />}

            {messenger && <Messenger />}
            {/* {isAdim && meetInfoPopup &&
                // <MeetingInfo setMeetInfoPopup={setMeetInfoPopup} a={meetInfoPopup} url={url} />}
            <Messenger /> */}
            <div style={{ position: 'absolute', top: 0, left: '45%' }}>
                <Request />
            </div>
            {record && <Record startRecording={startRecording} mediaBlobUrl={mediaBlobUrl} status={status} stopRecording={stopRecording} mode={mode} downloadVideo={downloadVideo} clearBlobUrl={clearBlobUrl} setMode={setMode} pauseRecording={pauseRecording} resumeRecording={resumeRecording} />}
        </div>

    )

}


const mapStateToProps = (state) => {
    return {
        stream: state.mainStream,
        participants: state.participants,
        currentUser: state.currentUser,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setMainStream: (stream) => dispatch(setMainStream(stream)),
        updateUser: (user) => dispatch(updateUser(user)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CallPage);
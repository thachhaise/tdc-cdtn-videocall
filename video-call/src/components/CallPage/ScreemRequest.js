import React, { useEffect, useRef, useState } from "react";
import './StyleCallPage/ScreemRequest.scss';
import { faStar, faBackward, faTemperatureHigh } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useHistory, Link } from 'react-router-dom';
import { ref, onValue, set } from '@firebase/database';
import databasedb from "../../sever/firebase";
import { useAuth } from "../../contexts/AuthContext";
import { green } from '@mui/material/colors';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import Fade from '@mui/material/Fade';
import Header from '../HomePage/Header/Navbar';
import MediaQuery from 'react-responsive';


const ScreemRequest = (props) => {

    const [loading, setLoading] = React.useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const timerRef = React.useRef();
    const videoRef = useRef(null);
    const [abc, setAbc] = useState(null);

    React.useEffect(
        () => () => {
            clearTimeout(timerRef.current);
        },

        []);
    if (typeof (Storage) !== 'undefined') {
        if (sessionStorage.reloadRequest) {
            let reloadRequest = sessionStorage.getItem('reloadRequest');
            if (reloadRequest == 0) {
                sessionStorage.setItem('reloadRequest', 1);
                window.location.reload();
            }
        } else {
            sessionStorage.setItem('reloadRequest', 1);
        }
    } else {
        alert("Không hỗ trợ");
    }


    useEffect(() => {
        setTimeout(function () {
            setIsLoading(!isLoading);
        }, 1000);

    }, []);

    const { currentUser } = useAuth();
    const id_user = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');
    const roomID = window.location.pathname.replaceAll('/', '');
    const history = useHistory();
    const [videoSrc, setVideoSrc] = useState(null);
    let username = null;

    const db = ref(databasedb, 'users/' + id_user + '/username');
    onValue(db, (snap) => {
        username = snap.val();
    });



    // let isAdd = false;
    let isAdd1 = false;
    let addCall = null;
    let admin = false;
    const [isAdd, setisAdd] = useState(false);
    const [bienfb, setbienfb] = useState(false);
    const [removeUser, setRemoveUser] = useState(false);
    const checkSize = ref(databasedb, `room/${roomID}/add/${id_user}`);
    const checkAdd = ref(databasedb, `room/${roomID}/add/${id_user}/addUser`);
    const checkAdmin = ref(databasedb, `room/${roomID}/admin`);
    const deleteUser = ref(databasedb, `room/${roomID}/remove/${id_user}/remove`);
    const deleteAdd = ref(databasedb, `room/${roomID}/remove/${id_user}/refuse`);
    const addRemove = ref(databasedb, `room/${roomID}/remove/${id_user}`);
    const [userAdmin, setUserAdmin] = useState(false);
    const [deleteUser1, setdeleteUser1] = useState(false);
    const [deleteAdd1, setdeleteAdd1] = useState(false);

    const getvideo = () => {
        navigator.mediaDevices

            .getUserMedia({
                video: { width: '100%', height: '100%' }
            })
            .then(stream => {
                let video = videoRef.current;
                video.srcObject = stream;
                video.play();
            })
            .catch(err => {
                console.log(err);
            })

    }
    useEffect(() => {
        getvideo();
    }, [])


    const startCall = () => {
        setLoading((prevLoading) => !prevLoading);
        // isAdd = true;
        console.log(isAdd1)
        isAdd1 = !isAdd1;
        console.log(isAdd1)

        if (isAdd == true) {
            setisAdd(false)
        } else {
            setisAdd(true);
            set(checkSize, null);
        }
        //    alert(isAdd)

        if (userAdmin == true) {
            sessionStorage.setItem('reloadRequest', 0);
            history.replace(`/meet/${roomID}`);
        } else {
            if (addCall == null) {
                if (loading == false) {
                    set(checkSize, { addUser: false, userName: username, email: id_user });
                    set(addRemove, { refuse: false, remove: false });
                } else { set(checkSize, null); }
            }
        }
    }

    if (isAdd == true && bienfb == true) {
        sessionStorage.setItem('reloadRequest', 0);
        history.replace(`/meet/${roomID}`);
    }
    useEffect(async () => {
        onValue(checkAdd, (snap) => {
            const add = snap.val();
            addCall = add;
            console.log(isAdd1);
            setbienfb(add);
            // if (isAdd1 == true && addCall == true) {
            //    setisAdd(false);
            //    isAdd1 = !isAdd1;

            //     // addCall = false;
            //     sessionStorage.setItem('reloadRequest', 0);
            //     history.replace(`/meet/${roomID}`);
            // }
        });

        onValue(checkAdmin, (snap) => {
            const val = snap.val();
            if (currentUser.email === val) {
                admin = true;
                setUserAdmin(admin);
            }
        });

        onValue(deleteAdd, (snap) => {
            const va = snap.val();
            if (va != null) {
                setdeleteAdd1(va);
            }

        });


        onValue(deleteUser, (snap) => {
            const va = snap.val();
            if (va != null) {
                setdeleteUser1(va);
            }



        });

    }, []);

    const leave = () => {
        history.replace('/');
    }

    // if (isLoading) {
    //     return (
    //         <div className="screem-request">
    //             <div className="bg">
    //                 <div class="loader-container">
    //                     <div class="loader"></div>
    //                 </div>
    //             </div>
    //         </div>
    //     )
    // }

    return (
        <div className="screem-request">
            <Header />
            <div className="body">


                <MediaQuery minWidth={769}>
                    <div className="video1">
                        <video ref={videoRef}></video>
                    </div>
                    <div className="right-side">
                        <div className="thamgia">
                            Sẵn sàng tham gia?
                        </div>
                        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>

                            {deleteUser1 === true &&
                                <h6>
                                    Bạn bị đuổi khỏi nhóm !!!
                                </h6>
                            }
                            {deleteUser1 === false &&
                                <div>
                                    {deleteAdd1 === true &&
                                        <h6>
                                            Bạn không được chấp nhận vào phòng !!!
                                        </h6>
                                    }
                                    {isAdd === true &&
                                        <h6>
                                            Đang yêu cầu tham gia
                                        </h6>
                                    }

                                    {deleteAdd1 == false &&

                                        <Box sx={{ height: 40, alignItems: 'center', flexDirection: 'column', display: 'flex', marginTop: 2, marginBottom: 2 }}>

                                            <Fade
                                                in={loading}
                                                style={{
                                                    transitionDelay: loading ? '800ms' : '0ms',
                                                }}
                                                unmountOnExit
                                            >
                                                <CircularProgress />
                                            </Fade>
                                        </Box>



                                    }
                                </div>
                            }

                            <button className="btn " onClick={startCall}>
                                {loading === true &&
                                    <h6>
                                        Hủy
                                    </h6>
                                }
                                {loading === false &&
                                    <h6>
                                        Yêu cầu tham gia
                                    </h6>
                                }
                            </button>


                            <button className="btn bac " onClick={leave}>
                                <FontAwesomeIcon className="icon-block" icon={faBackward} />
                                Quay về
                            </button>
                        </Box>
                    </div>
                </MediaQuery>

                <MediaQuery maxWidth={768}>
                    <div className="scr">
                        <div className="video1">
                            <video ref={videoRef}></video>
                        </div>

                        <div className="yeucau">

                            <div className="thamgia">
                                Sẵn sàng tham gia?
                            </div>
                            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>

                                {deleteUser1 === true &&
                                    <h6>
                                        Bạn bị đuổi khỏi nhóm !!!
                                    </h6>
                                }
                                {deleteUser1 === false &&
                                    <div>
                                        {deleteAdd1 === true &&
                                            <h6>
                                                Bạn không được chấp nhận vào phòng !!!
                                            </h6>
                                        }
                                        {isAdd === true &&
                                            <h6>
                                                Đang yêu cầu tham gia
                                            </h6>
                                        }

                                        {deleteAdd1 == false &&

                                            <Box sx={{ height: 40, alignItems: 'center', flexDirection: 'column', display: 'flex', marginTop: 2, marginBottom: 2 }}>

                                                <Fade
                                                    in={loading}
                                                    style={{
                                                        transitionDelay: loading ? '800ms' : '0ms',
                                                    }}
                                                    unmountOnExit
                                                >
                                                    <CircularProgress />
                                                </Fade>
                                            </Box>



                                        }
                                    </div>
                                }

                                <button className="btn " onClick={startCall}>
                                    {loading === true &&
                                        <h6>
                                            Hủy
                                        </h6>
                                    }
                                    {loading === false &&
                                        <h6>
                                            Yêu cầu tham gia
                                        </h6>
                                    }
                                </button>


                                <button className="btn bac " onClick={leave}>
                                    <FontAwesomeIcon className="icon-block" icon={faBackward} />
                                    Quay về
                                </button>
                            </Box>
                        </div>

                    </div>
                </MediaQuery>

            </div>
        </div >
    )
}

export default ScreemRequest;
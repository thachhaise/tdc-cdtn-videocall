import './ListCall.css';
import { Card, Table } from 'react-bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
import databasedb from "../../../sever/firebase";
import { useAuth } from "../../../contexts/AuthContext";
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';
import ListItem from './ListItem';
import './ListItem.css';
import { useEffect, useState } from "react";

const ListCall = () => {
    const { currentUser } = useAuth();
    const id_user = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');
    const links = ref(databasedb, `list/${id_user}/`);
    const [chatHistory, setChatHistory] = useState([]);
    let username = null;
    useEffect(() => {
        try {
            onValue(links, (snapshot) => {
                let childData = [];
                snapshot.forEach((childSnapshot) => {
                    childData.push(childSnapshot.val());
                });
                setChatHistory(childData);
            }, {
                onlyOnce: false
            })

        } catch (e) {

        }

    }, []);
    const listItems = chatHistory.map((number) =>
        <ListItem key={number.key} username={number.RoomId} />
    )

    return (
        <div>
            <div className="containers" >
                 
                <h4 style={{ color: "black",textAlign:'center' }}>
                    CUỘC HỌP ĐÃ THAM GIA
                </h4>
                <div className="listCall" id="style-2">
                    <div class="force-overflow">
                        {listItems}
                    </div>
                </div>
            </div>
        </div>


    )
}

export default ListCall;
import React from "react";
import { Card, Table } from "react-bootstrap";
import './ListItem.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
   faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import databasedb from "../../../sever/firebase";
import { useAuth } from "../../../contexts/AuthContext";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import { ref, onValue, orderByChild, push, set, onChildChanged, onChildAdded, child, onDisconnect, remove } from '@firebase/database';
import { useParams } from "react-router-dom";
const ListItem = ({ username }) => {
    const { id } = window.location.host;
    const { currentUser } = useAuth();
    const id_user = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');
    const link1 = ref(databasedb, `list/${id_user}/${username}`);
    const deleteTodo = () => {

        set(link1,null);

    };
    return (
        <div className="background-home">


            <Card class="shadow p-2 mb-6 bg-white rounded" style={{ height: '3rem', marginTop: "10px", textAlign: "center", fontWeight: "bold" }}>
                <Table striped bordered hover>
                        <tr>
                             <td>
                           
                                <Link style={{ textDecoration:'none'}}  to={ username} className="my-link">{window.location.host}/{username}</Link>
                             </td>
                             <td>
                                <div className>
                                    <button onClick={deleteTodo} >
                                     <FontAwesomeIcon className="icon" icon={faTrashAlt} />
                                      </button>
                                </div>
                             </td>   
                          </tr> 
                        
                            
                               
                           
                 
                </Table>
            </Card>

        </div>
    );
}

export default ListItem;
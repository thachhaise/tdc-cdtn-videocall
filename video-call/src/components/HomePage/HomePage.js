import { useHistory, Link } from 'react-router-dom';
import { faKeyboard, faVideo, faCheck, faKiss } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './HomePage.scss'
import { Card, Button, Alert } from "react-bootstrap"
import shortid from "shortid";
import Navbar from './Header/Navbar';
import React, { useState, useEffect } from "react"
import ListCall from './ListCall/ListCall';
import { useAuth } from "../../contexts/AuthContext"
import databasedb from '../../sever/firebase'
import { ref, child, onChildAdded, onChildChanged, onValue, update, push, set } from '@firebase/database';
import MediaQuery from 'react-responsive';

const HomePage = () => {
    const [isLoading, setLoading] = useState(true);
    if (typeof (Storage) !== 'undefined') {
        if (sessionStorage.reload) {
            let reload = sessionStorage.getItem('reload');
            if (reload == 1) {
                sessionStorage.setItem('reload', 0);
                window.location.reload();
            }
        } else {
            sessionStorage.setItem('reload', 0);
        }
    } else {
        alert("Không hỗ trợ");
    }

    useEffect(() => {
        setTimeout(function () {
            setLoading(!isLoading);
        }, 1000);

    }, []);

    const [error, setError] = useState("")
    const { currentUser, logout } = useAuth()
    const history = useHistory()

    const id = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');

    const startCall = () => {
        const uid = shortid.generate();
        const status = ref(databasedb, "room/" + uid);
        set(status, {
            status: true,
            admin: currentUser.email
        })
        history.replace(`/meet/${uid}`);
    }

    const [inputValue, setInputValue] = useState("");

    const onChangeHandler = event => {
        setInputValue(event.target.value);
    };

    const joinbtn = () => {
        history.push(`${inputValue}`);
    }

    const db = ref(databasedb, 'users/' + id + '/username');
    let name = null;

    onValue(db, (snap) => {
        name = snap.val();
    });

    if (isLoading) {
        return (
            
                <div className="bg">
                    <div class="loader-container">
                        <div class="loader"></div>
                    </div>
                </div>
            
        )
    }

    return (
        <div>
            <Navbar />
            <div className="home-page">

                <div className="body">

                    <MediaQuery maxWidth={768} >
                        <div className="left-side">
                            <div className="content">

                                <h2>Hội nghị truyền hình an toàn cho mọi người</h2>
                                <p>
                                    Để thấy nhiều người hơn cùng một lúc, hãy chuyển tới phần Thay đổi bố cục trong trình đơn Tùy chọn khác
                                </p>
                                <div className="action-btns">
                                    <button className="btn green" onClick={startCall}>
                                        <FontAwesomeIcon className="icon-block" icon={faVideo} />
                                        Cuộc họp mới
                                    </button>

                                </div>
                                <div className="input-block">
                                    <div className="input-section">
                                        <FontAwesomeIcon className="icon-block" icon={faKeyboard} />
                                        <input placeholder="Enter a code or link" onChange={onChangeHandler} value={inputValue} />
                                    </div>
                                    <button className="btn no-bg" onClick={joinbtn}>Join</button>
                                </div>
                                <ListCall />
                            </div>
                        </div>
                    </MediaQuery>
                    <MediaQuery minWidth={769} >
                        <div className="left-side">
                            <div className="content">

                                <h2>Hội nghị truyền hình an toàn cho mọi người</h2>
                                <p>
                                    Để thấy nhiều người hơn cùng một lúc, hãy chuyển tới phần Thay đổi bố cục trong trình đơn Tùy chọn khác
                                </p>
                                <div className="action-btns">
                                    <button className="btn green" onClick={startCall}>
                                        <FontAwesomeIcon className="icon-block" icon={faVideo} />
                                        Cuộc họp mới
                                    </button>
                                    <div className="input-block">
                                        <div className="input-section">
                                            <FontAwesomeIcon className="icon-block" icon={faKeyboard} />
                                            <input placeholder="Enter a code or link" onChange={onChangeHandler} value={inputValue} />
                                        </div>
                                        <button className="btn no-bg" onClick={joinbtn}>Join</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="right-side">
                            <ListCall />
                        </div>

                    </MediaQuery>




                </div>
            </div>
        </div>
    )
}

export default HomePage;
import React, { useState, useEffect } from 'react';
import './Navbar.css';
import logo from '../../../image/logo1.png'
import {
 faBars,faTimes,faUser
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useAuth } from "../../../contexts/AuthContext"
import databasedb from '../../../sever/firebase'
import { useHistory, Link } from 'react-router-dom';
import { ref, child, onChildAdded, onChildChanged, onValue, update, push } from '@firebase/database';
import { Card, Button, Alert } from "react-bootstrap"
import { Dropdown } from 'react-bootstrap';

function Navbar() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);
  // 
  const [error, setError] = useState("")
  const { currentUser, logout } = useAuth()
  const history = useHistory()
  let today = new Date();
  let day = "Th"+today.getDay()+","+today.getDate()+"Thg"+today.getMonth();
  let time = today.getHours() + ":" + today.getMinutes() 

  const id = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');

  const db = ref(databasedb, 'users/' + id + '/username');
  let name = null;

  onValue(db, (snap) => {
      name = snap.val();
  });

  async function handleLogout() {
      setError("")

      try {
          await logout()
          history.replace("/login")
      } catch {
          setError("không thể đăng xuất")
      }
  }
  // 

  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <div className="navbar-logo" onClick={closeMobileMenu}>
                <img src={logo} />
            </div>
          <div className='menu-icon' onClick={handleClick}>
             <FontAwesomeIcon className="icon" icon={click ?  faTimes: faBars}/>

          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
            <div className='action-btn'>
                <div className="date">
                {time} * {day}
                </div>
                <nav>
                    <p> <FontAwesomeIcon icon={faUser}/></p>
                    <input id="toggle" type="checkbox" defaultChecked />
                    <ul>
                    <li><Link style={{ textDecoration:'none'}}  to="/update-profile" className="kha">{currentUser.email}</Link></li>
                        <li><div className="kha"onClick={handleLogout} > Đăng Xuất </div></li>
                    
                    </ul>
                </nav>
            </div>
            </li>
          </ul>

         
        </div>
      </nav>
      
    </>
  );
}

export default Navbar;

import { useAuth } from "././contexts/AuthContext";
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router';
import { ref, push, onValue, set, child, onChildAdded, onChildChanged, onChildRemoved, onDisconnect } from '@firebase/database';
import databasedb from "./sever/firebase";
import CallPage from "./components/CallPage/CallPage";
import {
  setMainStream,
  addParticipant,
  setUser,
  removeParticipant,
  updateParticipant,
} from "./store/actioncreator";
import { connect } from "react-redux";

function Meet(props) {
  const [kiemTra, setKiemTra] = useState(false);
  const [isLoading, setLoading] = useState(true);

  const history = useHistory();
  let { id } = useParams();
  const isAdim = window.location.hash == "#init" ? true : false;
  const url = `${window.location.origin}${window.location.pathname}`;

  const { currentUser } = useAuth();
  const id_user = currentUser.email.replaceAll('.', '_').replaceAll('@', '_');
  const links = ref(databasedb, `list/${id_user}/${id}`);
  const db = ref(databasedb, 'users/' + id_user + '/username');
  const dbCheck = ref(databasedb, `room/${id}/status`);
  const checkAdd = ref(databasedb, `room/${id}/add/${id_user}/addUser`);
  const dbCheckAdmin = ref(databasedb, `room/${id}/admin`);

  if (currentUser) {

  } else {
    history.push(`/login/${id}`);
  }

  const getUserStream = async () => {
    const localStream = await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true,
    });

    return localStream;
  };

  useEffect(async () => {

    try {
      set(links, {
        RoomId: id
      });

    }
    catch {

    }

    const stream = await getUserStream();
    stream.getVideoTracks()[0].enabled = false;
    props.setMainStream(stream);

    onValue(dbCheckAdmin, (admin) => {
      const checkAdmin = admin.val();
      if (checkAdmin === currentUser.email) {
        setKiemTra(true);
        set(dbCheck, true).then(() => {
          onValue(dbCheck, (status) => {
            const check = status.val();
            if (check == true) {
              onValue(db, (snap) => {
                const username = snap.val();
                onValue(connectedRef, (snap) => {
                  if (snap.val()) {
                    const defaultPreference = {
                      audio: true,
                      video: false,
                      screen: false,
                      hand: false,
                      email: id_user,
                    };

                    const newPost = push(participantRef);
                    set(newPost, {
                      userName: username,
                      preferences: defaultPreference,
                    })

                    props.setUser({
                      [newPost.key]: { name: username, ...defaultPreference },
                    });

                    onDisconnect(newPost).remove();
                    setTimeout(function () {
                      setLoading(!isLoading);
                    }, 1000);
                  }
                });
              });
            }
          })
        }).catch(() => {
        });
      } else {
        setKiemTra(true);
        onValue(checkAdd, (add) => {
          const isAdd = add.val();
          if (isAdd === null || isAdd == false) {
            sessionStorage.setItem('reloadRequest', 0);
            history.replace({ pathname: `/${id}`, state: `${id}` });
          } else {
            onValue(dbCheck, (status) => {
              const check = status.val();
              if (check == true) {
                onValue(db, (snap) => {
                  const username = snap.val();
                  onValue(connectedRef, (snap) => {
                    if (snap.val()) {
                      const defaultPreference = {
                        audio: true,
                        video: false,
                        screen: false,
                        hand: false,
                        email: id_user,
                      };

                      const newPost = push(participantRef);
                      set(newPost, {
                        userName: username,
                        preferences: defaultPreference,
                      })

                      props.setUser({
                        [newPost.key]: { name: username, ...defaultPreference },
                      });

                      onDisconnect(newPost).remove();
                      setTimeout(function () {
                        setLoading(!isLoading);
                      }, 1000);
                    }
                  });
                });
              } else {
                sessionStorage.setItem('reload', 1);
                history.replace('/');
              }
            })
          }
        });
      }
    });
  }, []);

  const connectedRef = ref(databasedb, ".info/connected");
  const participantRef = ref(databasedb, "room/" + id + "/participants");

  const isUserSet = !!props.user;
  const isStreamSet = !!props.stream;

  useEffect(() => {
    if (isStreamSet && isUserSet) {
      onChildAdded(participantRef, (snap) => {
        onChildChanged(child(participantRef, snap.key + "/preferences"), (preferenceSnap) => {
          props.updateParticipant({
            [snap.key]: {
              [preferenceSnap.key]: preferenceSnap.val(),
            },
          });
        });
        const { userName: name, preferences = {} } = snap.val();
        props.addParticipant({
          [snap.key]: {
            name,
            ...preferences,
          },
        });
      });

      onChildRemoved(participantRef, (snap) => {
        props.removeParticipant(snap.key);
      });
    }
  }, [isStreamSet, isUserSet]);

  return (
    <div>
      <CallPage kiemTra={kiemTra} isLoading={isLoading} />
    </div>
  );


}


const mapStateToProps = (state) => {
  return {
    stream: state.mainStream,
    user: state.currentUser,
  };
};

const mapDispatchToProps = (dispatch) => {

  return {
    setMainStream: (stream) => dispatch(setMainStream(stream)),
    addParticipant: (user) => dispatch(addParticipant(user)),
    setUser: (user) => dispatch(setUser(user)),
    removeParticipant: (userId) => dispatch(removeParticipant(userId)),
    updateParticipant: (user) => dispatch(updateParticipant(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Meet);